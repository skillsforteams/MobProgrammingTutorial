# MobProgramming Tutorial

These is a mob programming tutorial started as a MobBlogging action from an Open Space Session on the DevOpsDays Copenhagen 4th of April 2019
https://twitter.com/jankrag/status/1113764595079024641

## Getting Started

These is a collection of best practices how to implement MobProgramming with your team

## Authors

* **Jan Krag** - *Initial Session* - [JanKrag](https://twitter.com/jankrag) [Praqma](https://www.praqma.com/)
* **John Behrens** - *Initial Session* - [JanKrag](https://twitter.com/jankrag) [Praqma](https://www.praqma.com/)

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.

## License

we did never talk about that

## Acknowledgments

https://github.com/emilybache

was inspired by a talk from EmilyBrache 
* **EmilyBrache** - *Mob Programming Coach* - [GitHub](https://github.com/emilybache) [Praqma](https://www.praqma.com/)

## Forked from
https://github.com/WebconsultsEU/DevOpsDaysCPH-MobBlogging

